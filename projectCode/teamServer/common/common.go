package common

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"math/rand"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/influxdata/influxdb/client/v2"
	"github.com/surgemq/message"
	"github.com/surgemq/surgemq/service"
	"gopkg.in/mgo.v2"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

// ServerToDeviceMsg : Message format for sending command to device
type ServerToDeviceMsg struct {
	Command   string
	Payload   interface{}
	Timestamp int64
}

// WsCommand : Message format for websocket message
type WsCommand struct {
	Event   string
	Payload string
}

// MqttListener : MQTT Server connector
var MqttListener *service.Client

// WebSocketConnList : Client Websocket connection list
var WebSocketConnList map[sessions.Session]*websocket.Conn

/* Service connector */

// ConnectMDB : return mongoDB connector
func ConnectMDB() *mgo.Session {
	mdb, err := mgo.Dial("mongoDB:27017")
	CheckErr(err)
	err = mdb.DB("SkyhawkPhase1").Login("staff", "n0th1n9n0n53n5e")
	CheckErr(err)
	return mdb
}

// ConnectInfluxDB : return InfluxDB connector
func ConnectInfluxDB() client.Client {
	influxConnnection, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     "http://iotServicePack:8086",
		Username: "staff",
		Password: "n0th1n9n0n53n5e",
	})
	CheckErr(err)
	return influxConnnection
}

// QueryInfluxDB : convenience function to query the influx database
func QueryInfluxDB(clnt client.Client, cmd string) (res []client.Result, err error) {
	q := client.Query{
		Command:  cmd,
		Database: "SkyhawkPhase1",
	}
	if response, err := clnt.Query(q); err == nil {
		if response.Error() != nil {
			return res, response.Error()
		}
		res = response.Results
	} else {
		return res, err
	}
	return res, nil
}

// ConnectToMQTTServer : MQTT connector and store connector as common.MqttListener
// return only connection error
func ConnectToMQTTServer() error {
	if MqttListener != nil {
		MqttListener.Disconnect()
	}
	MqttListener = &service.Client{}
	mqttMsg := message.NewConnectMessage()
	mqttMsg.SetUsername([]byte("staff"))
	mqttMsg.SetPassword([]byte("51<yk@3k2o18"))
	mqttMsg.SetWillQos(2)
	mqttMsg.SetVersion(3)
	mqttMsg.SetCleanSession(true)
	mqttMsg.SetClientId([]byte("backend"))
	mqttMsg.SetKeepAlive(60)
	mqttMsg.SetWillTopic([]byte("skyhawkPhase1"))
	mqttMsg.SetWillMessage([]byte("backend: reconnecting..."))
	return MqttListener.Connect("tcp://iotServicePack:1883", mqttMsg)
}

/* ---> Commonly used function <--- */

// CheckErr : print error if it's not nil
func CheckErr(err error) bool {
	if err != nil {
		fmt.Printf("[! - Error]:%s\n", err)
		return true
	}
	return false
}

//RequestFormatter : Format raw json string to golang map variable
func RequestFormatter(bytecode []byte) gin.H {
	var jsonStructure gin.H
	CheckErr(json.Unmarshal(bytecode, &jsonStructure))
	return jsonStructure
}

// NewWebsocketConnectionInfo : Save webSocket client connection with client info
func NewWebsocketConnectionInfo(session sessions.Session, wsConn *websocket.Conn) {
	if WebSocketConnList == nil {
		WebSocketConnList = make(map[sessions.Session]*websocket.Conn, 0)
	}
	WebSocketConnList[session] = wsConn
}

// StringInSlice : Check if string in slice
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// SHA-256 : Convert string to SHA-256
func SHA256(password string) string {
	hasher := sha256.New()
	hasher.Write([]byte(password))
	return fmt.Sprintf("%x", hasher.Sum(nil))
}

// BroadCastAll : Send message to every active socket
func BroadCastAll(event string, payload string) {
	wsJSON, _ := json.Marshal(WsCommand{
		Event:   event,
		Payload: payload,
	})
	println(len(WebSocketConnList))
	for _, conn := range WebSocketConnList {
		conn.WriteMessage(1, wsJSON)
	}
}

//RandString : Random chunks of string
func RandString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}
