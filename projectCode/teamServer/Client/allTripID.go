package Client

import (
	"fmt"

	"../common"
	"github.com/gin-gonic/gin"
)

// GetAllTripID : Query all tripid
func GetAllTripID(c *gin.Context) {
	jsonData := []string{}
	influxDB := common.ConnectInfluxDB()
	queryResult, err := common.QueryInfluxDB(influxDB, fmt.Sprintf("SHOW TAG VALUES FROM physicalLog WITH KEY =~ /.*/"))
	if common.CheckErr(err) {
		c.JSON(500, gin.H{
			"result": err.Error(),
		})
		return
	}
	if len(queryResult[0].Series) == 0 {
		c.JSON(200, gin.H{
			"result": []gin.H{},
		})
		return
	}
	for _, row := range queryResult[0].Series[0].Values {
		fmt.Println(row)
		jsonData = append(jsonData, row[1].(string))
	}
	c.JSON(200, gin.H{
		"result": jsonData,
	})
}
