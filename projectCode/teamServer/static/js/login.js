function login(username, password) {
  $.ajax({
    url: "/api/v1/login",
    type: 'POST',
    data: {
        username : username,
        password : password
    },
    dataType: 'JSON',
    success: function(response){
        if(response.success == true){
            window.location.href = "/main_system.html";
        }
        else {
            alert(response.errmsg);
        }
    }
});
}
