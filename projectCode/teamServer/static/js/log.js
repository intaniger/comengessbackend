function loadDeviceLog(tripID) {
  var chartConfig = {

    chart: {
        zoomType: 'x',
        marginLeft:50,
        position:{
          align:"center",
        }
    },

    title: {
        text: `Device log for tripID: ${tripID}`
    },
    tooltip: {
        valueDecimals: 2
    },

    xAxis: {
        type: 'datetime'
    },

    series: [
      // {
      //   data: data,
      //   lineWidth: 0.5,
      //   name: 'Hourly data points'
      // }
    ]

  }
  $.get(`/api/v1/deviceLog/physical/${tripID}`,
    function(data,status) {
      $("#logTable").html("");

      var graphSeries = {}
      var tableHead,tableRow,columnName="";
      for(columnName in data.result[0]) {
        tableHead += "<th>" + columnName + "</th>"
        graphSeries[columnName] = {
          lineWidth: 0.5,
          name: columnName,
          data:[],
        }
      }

      $("#logTable").append("<tr>"+tableHead+"</tr>");
      for(row in data.result){
          tableRow += "<tr>"
          for(columnName in data.result[row]) {
              tableRow += "<td>" + data.result[row][columnName] + "</td>" ;
              graphSeries[columnName].data.push([data.result[row]["time"],parseFloat(data.result[row][columnName])])
          }
          tableRow+= "</tr>";
      }
      $("#logTable").append(tableRow);
      for (const columnName in graphSeries) {
        if (graphSeries.hasOwnProperty(columnName)) {
          chartConfig.series.push(graphSeries[columnName])
        }
      }
      Highcharts.chart('graphArea',chartConfig)
    }
  );
}
