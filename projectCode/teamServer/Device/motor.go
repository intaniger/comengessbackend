package Device

import (
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/surgemq/message"

	"../common"
)

// SetMotor : Set Device motor speed
func SetMotor(inputValue string) error {
	speedValue := calculateSpeed(inputValue)
	if speedValue == 1000 {
		return errors.New("speedValue should be integer and between -255 and 255")
	}
	motorJSON, _ := json.Marshal(common.ServerToDeviceMsg{
		Command:   "Motor",
		Payload:   speedValue,
		Timestamp: time.Now().UnixNano() / int64(time.Second),
	})
	publishMsg := message.NewPublishMessage()
	publishMsg.SetTopic([]byte("skyhawkPhase1"))
	publishMsg.SetPayload([]byte(string(motorJSON)))
	publishMsg.SetQoS(2)
	return common.MqttListener.Publish(publishMsg, nil)
}

func calculateSpeed(inputValue string) int {
	rawValue, err := strconv.Atoi(inputValue)
	if err != nil {
		rawValue = 1000
	}
	outputValue := 0
	if rawValue < -255 || rawValue > 255 {
		outputValue = 1000
	} else {
		outputValue = rawValue
	}
	return outputValue
}
