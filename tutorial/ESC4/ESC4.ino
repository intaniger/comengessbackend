/**
 * Usage, according to documentation(https://www.firediy.fr/files/drone/HW-01-V4.pdf) : 
 *     1. Plug your Arduino to your computer with USB cable, open terminal, then type 1 to send max throttle to every ESC to enter programming mode
 *     2. Power up your ESCs. You must hear "beep1 beep2 beep3" tones meaning the power supply is OK
 *     3. After 2sec, "beep beep" tone emits, meaning the throttle highest point has been correctly confirmed
 *     4. Type 0 to send min throttle
 *     5. Several "beep" tones emits, which means the quantity of the lithium battery cells (3 beeps for a 3 cells LiPo)
 *     6. A long beep tone emits meaning the throttle lowest point has been correctly confirmed
 *     7. Type 2 to launch test function. This will send min to max throttle to ESCs to test them
 *
 * @author lobodol <grobodol@gmail.com>
 */
// ---------------------------------------------------------------------------
#include <Servo.h>
#include "TimerOne.h"
// ---------------------------------------------------------------------------
// Customize here pulse lengths as needed
#define MIN_PULSE_LENGTH 700 // Minimum pulse length in µs
#define MAX_PULSE_LENGTH 2000 // Maximum pulse length in µs
// ---------------------------------------------------------------------------
Servo motD;
char data;
long int counter = 0;
unsigned long long int prevMilliseconds = 0;
// ---------------------------------------------------------------------------
void docount()  // counts from the speed sensor
{
  counter++;  // increase +1 the counter value
}
/**
 * Initialisation routine
 */
void setup() {
    Serial.begin(9600);

    motD.attach(9, MIN_PULSE_LENGTH, MAX_PULSE_LENGTH);
    int start = millis();
    while(millis() - start < 5000){
      motD.writeMicroseconds(MIN_PULSE_LENGTH);
    }
    attachInterrupt(digitalPinToInterrupt(19), docount, RISING);
    takeOff();
    prevMilliseconds = millis();
//    displayInstructions();
}

/**
 * Main function
 */
void loop() {
  motD.writeMicroseconds(1650);
  if( abs(millis() - prevMilliseconds) >= 60000){
    Serial.print(String(counter));
    Serial.println(" rpm");
    prevMilliseconds = millis();
    counter = 0;
  }
}

/**
 * Test function: send min throttle to max throttle to each ESC.
 */
void takeOff()
{
    for (int i = 1500; i <= 1650; i += 1) {
        Serial.print("Pulse length = ");
        Serial.println(i);
        motD.writeMicroseconds(i);
    }
//    for (int i = 1650; i <= 1700; i += 10) {
//        Serial.print("Pulse length = ");
//        Serial.println(i);
//        motD.writeMicroseconds(i);
//    }
    Serial.println("Taked off");
//  motD.writeMicroseconds(1600);
}

/**
 * Displays instructions to user
 */
void displayInstructions()
{  
    Serial.println("READY - PLEASE SEND INSTRUCTIONS AS FOLLOWING :");
    Serial.println("\t0 : Send min throttle");
    Serial.println("\t1 : Send max throttle");
    Serial.println("\t2 : Run test function\n");
}

